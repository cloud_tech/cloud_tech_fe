#!/usr/bin/env sh

set -ex

echo >&3 "Generating .envfrom .env.template"
echo >&3 "API_URL set to $API_URL"

ls -la /app
envsubst < /app/env.template.js > /app/env.js

exit 0
