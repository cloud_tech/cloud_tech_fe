import CsrfTokenDto from "@/model/CsrfTokenDto";
import Axios from "axios";

const instance = Axios.create({
  baseURL: window.env.apiUrl
});

const CSRF_HEADER_NAME = "CSRF_HEADER_NAME";
const CSRF_TOKEN = "CSRF_TOKEN_VALUE";

localStorage.setItem(CSRF_HEADER_NAME, "");
localStorage.setItem(CSRF_TOKEN, "");

export function refreshCsrfToken(): void {
  instance.request<CsrfTokenDto>({
    method: "GET",
    url: window.env.apiUrl + "/csrf",
    withCredentials: true

  }).then(response => {
    const token = response.data;
    localStorage.setItem(CSRF_HEADER_NAME, token.headerName);
    localStorage.setItem(CSRF_TOKEN, token.token);

  }).catch(reason => {
    console.log("didn't fetch csrf token, reason:")
    console.log(reason);
  })
}

instance.interceptors.request.use(config => {
  const header = localStorage.getItem(CSRF_HEADER_NAME);
  const token = localStorage.getItem(CSRF_TOKEN);

  if (header === null
    || token == null
    || header.trim().length == 0
    || token.trim().length == 0) return config;

  config.headers = { [header]: token };
  return config;
});

export default instance;
