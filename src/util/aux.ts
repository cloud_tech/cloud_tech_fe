import { AxiosRequestConfig } from "axios";
import Axios from "@/util/axios";

declare global {
  interface EnvVars {
    apiUrl: string;
  }
  interface Window {
    env: EnvVars;
  }
}

export const VK_AUTH = "/oauth2/authorization/vk";
export const USER_INFO = "/me";
export const USER_FRIENDS = "/friends";
export const LOGOUT = "/logout";
export const WATCH = "/watch";

export function resource<T = any>(config: AxiosRequestConfig): Promise<T> {
  config.withCredentials = true;
  return Axios.request<T>(config).then(response => response.data);
}
