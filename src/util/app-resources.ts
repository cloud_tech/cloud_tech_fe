import { LOGOUT, resource, USER_FRIENDS, USER_INFO, WATCH } from "./aux";
import UserDetailsDto from "@/model/UserDetailsDto";

export const User = {
  info(): Promise<UserDetailsDto> {
    return resource<UserDetailsDto>({
      method: "GET",
      url: window.env.apiUrl + USER_INFO
    });
  },

  logout(): Promise<void> {
    return resource<void>({
      method: "POST",
      url: window.env.apiUrl + LOGOUT
    });
  },

  friends(): Promise<UserDetailsDto[]> {
    return resource<UserDetailsDto[]>({
      method: "GET",
      url: window.env.apiUrl + USER_FRIENDS
    });
  },

  watch(id: string): Promise<void> {
    return resource<void>({
      method: "POST",
      url: window.env.apiUrl + WATCH,
      data: { id: id },
      // withCredentials: true
    });
  }
};
