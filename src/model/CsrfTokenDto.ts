export default interface CsrfTokenDto {
    headerName: string,
    token: string
}
