export default interface ActivityDto {
    online: boolean;
    duration: number;
    prettyDuration: string;
}