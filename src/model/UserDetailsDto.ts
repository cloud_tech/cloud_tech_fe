import ActivityDto from "./ActivityDto";

export default interface UserDetailsDto {
  id: string;
  firstName: string;
  lastName: string;
  photoUrl: string;
  isWatched: boolean;
  activity: ActivityDto[];
}
