import UserDetailsDto from "@/model/UserDetailsDto";
import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";
import { User } from "@/util/app-resources";
import store from "@/store/store";
import { refreshCsrfToken } from "@/util/axios";
import ActivityDto from "@/model/ActivityDto";

const SET_CURRENT_USER = "SET_CURRENT_USER";
const SET_FRIENDS = "SET_FRIENDS";

function prettyActivity(user: UserDetailsDto) {
  console.log("called");
  user.activity.forEach((a: ActivityDto) => {
    const hours = Math.floor(a.duration / 3600);
    const minutes = Math.floor((a.duration - hours * 3600) / 60);
    const seconds = Math.floor(a.duration % 60);

    const hPart = hours > 0 ? `${hours}h ` : "";
    const mPart = minutes > 0 ? `${minutes}m ` : "";
    const sPart = seconds > 0 ? `${seconds}s` : "";

    a.prettyDuration = hPart + mPart + sPart;
  })
}

@Module({ dynamic: true, store, name: "user" })
export default class UserModule extends VuexModule {
  currentUser: UserDetailsDto | null = null;
  friends: UserDetailsDto[] = [];

  @Mutation
  [SET_CURRENT_USER](newUser: UserDetailsDto | null) {
    this.currentUser = newUser;
  }

  @Mutation
  [SET_FRIENDS](friends: UserDetailsDto[]) {
    this.friends = friends;
  }

  @Action
  loadCurrentUser() {
    User
      .info()
      .then(info => this.context.commit(SET_CURRENT_USER, info));

    User
      .friends()
      .then(friends => {
        friends.forEach(f => prettyActivity(f));
        return friends;
      })
      .then(friends => this.context.commit(SET_FRIENDS, friends));
  }

  @Action
  logout() {
    User
      .logout()
      .finally(() => {
        this.context.commit(SET_CURRENT_USER, null);
        refreshCsrfToken();
      });
  }
}