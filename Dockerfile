# base
FROM docker.io/node:latest as base
ENV PROJECT_HOME /opt/cloud_tech_fe
WORKDIR $PROJECT_HOME

# deps & build
FROM base as deps
COPY package*.json ./
# RUN npm set progress=false
RUN npm set registry https://registry.npmjs.org/
RUN npm install

# build
FROM deps as build
COPY ./ .
RUN npm run build

# nginx
FROM docker.io/nginx:1.19.5-alpine
COPY --from=build /opt/cloud_tech_fe/dist/ /app
COPY 11-replace-api-url.sh /docker-entrypoint.d
